# Path Finding

## Description
Utilizes the A* Algorithm to navigate a sprite around a map.

## Badges
On some READMEs, you may see small images that convey metadata, such as whether or not all the tests are passing for the project. You can use Shields to add some to your README. Many services also have instructions for adding a badge.

## Visuals
Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

## Authors and acknowledgment
The Core Game Engine is provided curtosey of Dr. Santiago Ontanon

A* Implementation by Christopher S. Good, Jr. 
