package s3.ai;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.PriorityQueue;

import s3.base.S3;
import s3.entities.S3PhysicalEntity;
import s3.util.Pair;


public class AStar {
	
	/** Provided Coordinates */
	public Pair<Double, Double> m_start_coords, m_goal_coords;

	/** Provided Game Features */
	public S3PhysicalEntity m_entity;
	public S3 m_game;
	
	/**
	 * Static Method to calculate the distance of the path from the Starting 
	 * Position to the Goal Position on the given Game Instance's Map
	 * @param start_x Entity Starting X Coordinate
	 * @param start_y Entity Starting Y Coordinate
	 * @param goal_x Destination X Coordinate
	 * @param goal_y Destination Y Coordinate
	 * @param i_entity Desired Entity 
	 * @param the_game Game Instance
	 * @return Numerical value of the distance of the Desired Path
	 */
	public static int pathDistance(double start_x, double start_y, double goal_x, double goal_y,
		S3PhysicalEntity i_entity, S3 the_game) {
		AStar a = new AStar(start_x,start_y,goal_x,goal_y,i_entity,the_game);
		List<Pair<Double, Double>> path = a.computePath();
		if (path!=null) return path.size();
		return -1;
	}

	/**
	 * Implementation of the A* Algorithm to control Entity Motion
	 * @param start_x Entity Starting X Coordinate
	 * @param start_y Entity Starting Y Coordinate
	 * @param goal_x Destination X Coordinate
	 * @param goal_y Destination Y Coordinate
	 * @param i_entity Desired Entity 
	 * @param the_game Game Instance
	 */
	public AStar(double start_x, double start_y, double goal_x, double goal_y,
		S3PhysicalEntity i_entity, S3 the_game) {
		m_start_coords = new Pair<Double, Double>(start_x, start_y);
		m_goal_coords = new Pair<Double, Double>(goal_x, goal_y);
		m_entity = i_entity;
		m_game = the_game;
	}

	/**
	 * Heuristic that captures the distance of the provided location from the goal
	 * @param current Current Location
	 * @return Distance to the Goal State
	 */
	public double distance(Pair<Double, Double> coords) {
		return Math.sqrt(
			Math.pow(this.m_goal_coords.m_a - coords.m_a, 2) + 
			Math.pow(this.m_goal_coords.m_b - coords.m_b, 2));
	} 

	/**
	 * Backtrack through the parents of the final node to capture the optimal 
	 * path that the entity should follow
	 * @param node Goal Node
	 * @return Desired path towards goal
	 */
	public List<Pair<Double, Double>> getOptimalPath(Node node) {
		List<Pair<Double, Double>> path = new ArrayList<>();
		Node current = node;
		/** When the Parent is null we know we have reached the start */
		while (current.m_parent != null) {
			path.add(current.m_coords);
			current = current.m_parent;
		}
		/** Convert path from Goal->Start to Start->Goal */
		Collections.reverse(path);
		return path;
	} 

	/**
	 * Find the path that the Entity should traverse to reach the goal by 
	 * implementing the A* Path-finding Algorithm
	 * 
	 * Note: Utilizes a Priority Queue to "pop" the lowest F value
	 * @return
	 */
	public List<Pair<Double, Double>> computePath() {

		/** Initialize the Start and Goal Nodes */
		Node start = new Node(m_start_coords, 0, this.distance(m_start_coords), null);
		Node goal = new Node(m_goal_coords, 0, 0, null);

		/** Initialize the Open and Closed Lists */
		PriorityQueue<Node> openNodes = new PriorityQueue<>();
		List<Node> closedNodes = new ArrayList<>();

		/** Push Start Node to the front of the Queue */
		openNodes.add(start);

		/** Iterate through all possible Open Nodes */
		while (!openNodes.isEmpty()) {

			/** 
			 * Remove the Node with the lowest F value 
			 * Note: Using Poll to avoid throwing errors, the base
			 * condition of the While-loop will handle those
			 */
			Node N = openNodes.poll();

			/** If the goal has been found, return the path */
			if (N.equals(goal)) return getOptimalPath(N);

			/** 
			 * If the goal has not been found, add current node to
			 * the Closed List
			 */
			closedNodes.add(N);

			/** 
			 * Run through all neighboring nodes 
			 * This is equivalent to traversing the children of 
			 * the Current Node (N)
			 *  -------------------------
			 * | -1, -1 | 0, -1 | 1, -1 |
			 *  -------------------------
			 * | -1, 0  | X, Y  | 1, 0  |
			 *  -------------------------
			 * | -1, 1  | 0, 1  | 1, 1  |
			 *  -------------------------
			 */
			for (int x = -1; x <= 1; x++) {
				for (int y = -1; y <= 1; y++) {
					if (x == y) continue; // Skip the current node (center)
					Pair<Double, Double> currentCoords = new Pair<Double, Double>(N.m_coords.m_a + x, N.m_coords.m_b + y);
					Node M = new Node(currentCoords, N.m_g + 1, this.distance(currentCoords), N);
					/**
					 * The Node can be skipped if it is either:
					 * - In the Open List
					 * - In the Closed List
					 * - Not an available space to move to
					 * 
					 * If it does not meet the above criteria, then it can be added to 
					 * the Open List to explore the Node and subsequently added as a 
					 * Child Node for the Current Node (N)
					 */
					if (!openNodes.contains(M) 
						&& !closedNodes.contains(M)
						&& this.m_game.isSpaceFree(1, M.m_coords.m_a.intValue(), M.m_coords.m_b.intValue())) {
						openNodes.add(M);
						N.m_children.add(M);
					}
				}
			}
		}

		/** Return null as a base-case for A* not running */
		return null;
	}
}
