package s3.ai;

import java.util.ArrayList;
import java.util.List;

import s3.util.Pair;

public class Node implements Comparable<Node> {

    /** Coordinates for Node */
    public Pair<Double, Double> m_coords;

    /** Calculated Values for Node */
    public double m_g, m_h, m_f;

    /** Parent/Child Relationship */
    public Node m_parent;
    public List<Node> m_children = new ArrayList<>();

    /**
     * Utility Class that defines a typical Node for the A* Algorithm
     * @param coords Coordinates of the Node on the Map
     * @param g Distance from the Starting Node
     * @param h Distance to the Goal
     * @param parent Direct Parent Node 
     */
    public Node(Pair<Double, Double> coords, double g, double h, Node parent) {
        m_coords = coords;
        m_g = g;
        m_h = h;
        m_f = g + h;
        m_parent = parent;
    }

    @Override
    public boolean equals(Object o) {
        /** Compare Object Equality */
        if (this == o) return true;

        /** Compare Coordinate Equality */
        Node node = (Node) o;
        return Double.compare(this.m_coords.m_a, node.m_coords.m_a) == 0 
            && Double.compare(this.m_coords.m_b, node.m_coords.m_b) == 0;
    }

    @Override
    public int compareTo(Node n) {
        /** Define the comparison parameters for the Priority Queue */
        if (this.m_f > n.m_f) { 
            return 1; 
        } else if (this.m_f < n.m_f) { 
            return -1; 
        } else { 
            return 0; 
        }
    }
}
